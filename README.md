Character Sheet for Dungeons & Dragons 5th edition
==================================================

These are the files I use to create a character sheet for the 5th edition
of Dungeons & Dragons. The PDF file is an original character sheet file.
The score details file is created by me and is publised under the CC BY 4.0
license. It can be used to keep track of all the scores in the character
sheet and how they are calculated.
